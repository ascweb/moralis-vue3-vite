import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import inject from '@rollup/plugin-inject';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [ vue({
        include: [/\.vue$/],
    })],
        resolve: {
            alias: {
                '/@src/': `/src/`,
                process: 'process/browser',
                zlib: 'browserify-zlib',
                util: 'util',
                web3: path.resolve(__dirname, './node_modules/web3/dist/web3.min.js'), // fix process error on DEV
            },
        },
        build: {
            rollupOptions: {
                plugins: [
                    inject({ Buffer: ['Buffer', 'Buffer'] }),
                    inject({ Buffer: ['process', 'process'] }),
                ],
            },
        },
})
