# Moralis + Vue 3 + Vite TEST

`npm init @vitejs/app moralis-test`
> https://github.com/MoralisWeb3/demo-apps/tree/main/moralis-vue3-boilerplate

## Updates from boilerplate

- npm install moralis and dependencies

- copy  folder plugins
- copy folder store
- update main.js
- update App.vue

To solve process and buffer errors use:
`npm i @rollup/plugin-inject` then update vite.config.js adding:


     resolve: {
       alias: {
          web3: path.resolve(__dirname, './node_modules/web3/dist/web3.min.js'), // fix process error on DEV
         },
       },



### ERRORS ON DEV

- `process` is not defined in Vite
  > https://vitejs.dev/guide/env-and-mode.html#env-variables
  
     * Into `src/plugins/moralis.js` to load `.ENV` variables I changed `process.env.VUE_APP_MORALIS_SERVER_URL` to `import.meta.env.VITE_APP_MORALIS_SERVER_URL`
     * Console error: `ReferenceError: process is not defined` in moralis module. To fix it take a look at 
     * Note: `https://github.com/vitejs/vite/issues/512`


### ERRORS ON BUILD

- `Uncaught ReferenceError: Buffer is not defined`
    
